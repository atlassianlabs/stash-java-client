package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.MoreObjects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Basic representation of a repository mirror
 */
public class MirrorServer {
    private final String selfUrl;
    private final String baseUrl;
    private final String id;
    private final String name;
    private final boolean enabled;
    private final String type;
    private final String productType;
    private final String productVersion;
    private final Long lastSeenDate;

    public MirrorServer(@Nullable String selfUrl,
                        @Nullable String baseUrl,
                        @Nonnull String id,
                        @Nonnull String name,
                        boolean enabled,
                        @Nullable String type,
                        @Nullable String productType,
                        @Nullable String productVersion,
                        @Nullable Long lastSeenDate) {
        this.selfUrl = selfUrl;
        this.baseUrl = baseUrl;
        this.id = id;
        this.name = name;
        this.enabled = enabled;
        this.type = type;
        this.productType = productType;
        this.productVersion = productVersion;
        this.lastSeenDate = lastSeenDate;
    }

    @Nullable
    public String getSelfUrl() {
        return selfUrl;
    }

    @Nullable
    public String getBaseUrl() {
        return baseUrl;
    }

    @Nonnull
    public String getId() {
        return id;
    }

    @Nonnull
    public String getName() {
        return name;
    }

    @Nullable
    public String getType() {
        return type;
    }

    @Nullable
    public String getProductType() {
        return productType;
    }

    @Nullable
    public String getProductVersion() {
        return productVersion;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @Nullable
    public Long getLastSeenDate() {
        return lastSeenDate;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("selfUrl", selfUrl)
                .add("baseUrl", baseUrl)
                .add("id", id)
                .add("name", name)
                .add("enabled", enabled)
                .add("type", type)
                .add("productType", productType)
                .add("productVersion", productVersion)
                .add("lastSeenDate", lastSeenDate)
                .toString();
    }
}
