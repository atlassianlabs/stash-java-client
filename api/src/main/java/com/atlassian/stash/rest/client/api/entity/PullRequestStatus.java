package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.MoreObjects;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

/**
 * pull request state
 */
public class PullRequestStatus {
    private final long id;
    private final long version;
    private final String state;
    @Nonnull
    private final String title;
    @Nonnull
    private final Optional<String> description;
    @Nonnull
    private final String url;
    @Nonnull
    private final Optional<String> mergeOutcome;
    @Nonnull
    private final PullRequestParticipant author;
    @Nonnull
    private final List<PullRequestParticipant> reviewers;
    @Nonnull
    private final PullRequestRef fromRef;
    @Nonnull
    private final PullRequestRef toRef;

    private final long lastUpdated;
    @Nonnull
    private final Optional<Long> commentCount;
    @Nonnull
    private final Optional<Long> outstandingTaskCount;
    @Nonnull
    private final Optional<Long> resolvedTaskCount;

    public PullRequestStatus(long id,
                             long version,
                             @Nonnull final String title,
                             @Nonnull final Optional<String> description,
                             @Nonnull final String url,
                             @Nonnull String state,
                             @Nonnull final Optional<String> mergeOutcome, @Nonnull final PullRequestParticipant author,
                             @Nonnull final List<PullRequestParticipant> reviewers, @Nonnull final PullRequestRef fromRef,
                             @Nonnull final PullRequestRef toRef, final long lastUpdated,
                             @Nonnull final Optional<Long> commentCount,
                             @Nonnull final Optional<Long> outstandingTaskCount,
                             @Nonnull final Optional<Long> resolvedTaskCount) {
        this.id = id;
        this.version = version;
        this.title = title;
        this.description = description;
        this.url = url;
        this.state = state;
        this.mergeOutcome = mergeOutcome;
        this.author = author;
        this.reviewers = reviewers;
        this.fromRef = fromRef;
        this.toRef = toRef;
        this.lastUpdated = lastUpdated;
        this.commentCount = commentCount;
        this.outstandingTaskCount = outstandingTaskCount;
        this.resolvedTaskCount = resolvedTaskCount;
    }

    /**
     * @return the repository-scoped identifier of the pull request
     */
    public long getId() {
        return id;
    }

    /**
     * @return the current version of the pull request
     */
    public long getVersion() {
        return version;
    }

    /**
     * @return the mandatory title of the pull request
     */
    @Nonnull
    public String getTitle() {
        return title;
    }

    /**
     * @return the description of the pull request
     */
    @Nonnull
    public Optional<String> getDescription() {
        return description;
    }

    /**
     * @return url of this pull request
     */
    @Nonnull
    public String getUrl() {
        return url;
    }

    /**
     *
     * @return state either OPEN, DECLINED or MERGED
     */
    @Nonnull
    public String getState() {
        return state;
    }

    /**
     * Note this is optional as old Stash/Bitbucket Server versions may not return it (e.g. 3.0.8).
     * @return as of BBS 4.12.0 allowed values are <code>CLEAN</code>, <code>CONFLICTED</code>, <code>UNKNOWN</code>
     * @since Bitbucket 4.10.0
     */
    @Nonnull
    public Optional<String> getMergeOutcome() {
        return mergeOutcome;
    }

    /**
     * @return the creating author of the pull request
     */
    @Nonnull
    public PullRequestParticipant getAuthor() {
        return author;
    }

    /**
     * @return the reviewing participants attached to the pull request, unordered.
     */
    @Nonnull
    public List<PullRequestParticipant> getReviewers() {
        return reviewers;
    }

    /**
     * @return the ref containing the changes to review and merge
     */
    @Nonnull
    public PullRequestRef getFromRef() {
        return fromRef;
    }

    /**
     * @return the ref the changes should be merged to
     */
    @Nonnull
    public PullRequestRef getToRef() {
        return toRef;
    }

    /**
     * @return the last update of the pull request
     */
    public long getLastUpdated() {
        return lastUpdated;
    }

    /**
     * Note this is optional as Stash/Bitbucket Server seems to return no counter value if there are no comments.
     * @return total number of comments on pull request
     */
    @Nonnull
    public Optional<Long> getCommentCount() {
        return commentCount;
    }

    /**
     * Note this is optional as old Stash/Bitbucket Server versions may not return it (e.g. 3.0.8)
     * @return number of outstanding tasks
     * @since Bitbucket 4.3.0
     */
    @Nonnull
    public Optional<Long> getOutstandingTaskCount() {
        return outstandingTaskCount;
    }

    /**
     * Note this is optional as old Stash/Bitbucket Server versions may not return it (e.g. 3.0.8)
     * @return number of resolved tasks
     * @since Bitbucket 4.3.0
     */
    @Nonnull
    public Optional<Long> getResolvedTaskCount() {
        return resolvedTaskCount;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("version", version)
                .add("title", title)
                .add("description", description)
                .add("url", url)
                .add("state", state)
                .add("mergeOutcome", mergeOutcome)
                .add("author", author)
                .add("reviewers", reviewers)
                .add("fromRef", fromRef)
                .add("toRef", toRef)
                .add("lastUpdated", lastUpdated)
                .add("commentCount", commentCount)
                .add("outstandingTaskCount", outstandingTaskCount)
                .add("resolvedTaskCount", resolvedTaskCount)
                .toString();
    }
}
