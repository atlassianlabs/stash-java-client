package com.atlassian.stash.rest.client.api.entity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * A single entry of a {@link Report}'s custom data.
 */
public class ReportDataEntry {
    /**
     * Type of the value of the entry.
     */
    public enum Type {
        BOOLEAN, DATE, DURATION, LINK, NUMBER, PERCENTAGE, TEXT
    }

    @Nonnull
    private final String title;
    @Nonnull
    private final Object value;
    @Nullable
    private final Type type;

    public ReportDataEntry(@Nonnull String title, @Nonnull Object value, @Nullable Type type) {
        this.title = title;
        this.value = value;
        this.type = type;
    }

    /**
     * Display name of the data entry.
     */
    @Nonnull
    public String getTitle() {
        return title;
    }

    /**
     * Value of the data entry. Its type depends on the {@link #getType() type} used.
     *
     * @see Type
     */
    @Nonnull
    public Object getValue() {
        return value;
    }

    /**
     * Type of value defined by this data entry. Optional - if not provided, the type of the {@link #getValue() value}
     * will be interpreted based on its content.
     */
    @Nullable
    public Type getType() {
        return type;
    }
}
