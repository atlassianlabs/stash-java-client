package com.atlassian.stash.rest.client.api.entity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Represents a single code annotation belonging to a single {@link Report} for a particular file.
 */
public class CodeAnnotation {
    /**
     * Severity of the problem.
     */
    public enum Severity {
        LOW, MEDIUM, HIGH
    }

    /**
     * Type of the problem.
     */
    public enum Type {
        VULNERABILITY, CODE_SMELL, BUG
    }

    @Nonnull
    private final String reportKey;
    @Nullable
    private final String externalId;
    @Nonnull
    private final Severity severity;
    @Nonnull
    private final String message;
    @Nonnull
    private final String path;
    @Nullable
    private final Integer line;
    @Nullable
    private final String link;
    @Nullable
    private final Type type;

    public CodeAnnotation(@Nonnull String reportKey,
                          @Nullable String externalId,
                          @Nonnull Severity severity,
                          @Nonnull String message,
                          @Nonnull String path,
                          @Nullable Integer line,
                          @Nullable String link,
                          @Nullable Type type) {
        this.reportKey = reportKey;
        this.externalId = externalId;
        this.severity = severity;
        this.message = message;
        this.path = path;
        this.line = line;
        this.link = link;
        this.type = type;
    }

    /**
     * Key of the {@link Report} to which this code annotation belongs.
     */
    @Nonnull
    public String getReportKey() {
        return reportKey;
    }

    /**
     * Unique externally managed Id of the annotation. Optional. Arbitrarily assigned during annotation creation.
     */
    @Nullable
    public String getExternalId() {
        return externalId;
    }

    /**
     * Tells how severe the problem described by this code annotation is.
     */
    @Nonnull
    public Severity getSeverity() {
        return severity;
    }

    /**
     * Message associated with this code annotation.
     */
    @Nonnull
    public String getMessage() {
        return message;
    }

    /**
     * Path in the repository for the file to which the annotation applies.
     */
    @Nonnull
    public String getPath() {
        return path;
    }

    /**
     * Line number in the file from {@link #getPath()}. If not provided, the annotation applies to the entire file.
     */
    @Nullable
    public Integer getLine() {
        return line;
    }

    /**
     * External link for the code annotation (tool report, vendor page, etc.).
     */
    @Nullable
    public String getLink() {
        return link;
    }

    /**
     * Tells the type of the problem described by this code annotation.
     */
    @Nullable
    public Type getType() {
        return type;
    }
}
