package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.MirrorServer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.function.Function;

public class MirrorServerParser implements Function<JsonElement, MirrorServer> {
    @Override
    public MirrorServer apply(final JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();
        return getMirrorServer(jsonObject, null);
    }

    protected MirrorServer getMirrorServer(@Nonnull JsonObject mirrorServer, @Nullable String selfUrl) {
        return new MirrorServer(
                selfUrl,
                mirrorServer.has("baseUrl") ? mirrorServer.get("baseUrl").getAsString() : selfUrl,
                mirrorServer.get("id").getAsString(),
                mirrorServer.get("name").getAsString(),
                mirrorServer.get("enabled").getAsBoolean(),
                mirrorServer.has("mirrorType") ? mirrorServer.get("mirrorType").getAsString() : null,
                mirrorServer.has("productType") ? mirrorServer.get("productType").getAsString() : null,
                mirrorServer.has("productVersion") ? mirrorServer.get("productVersion").getAsString() : null,
                mirrorServer.has("lastSeenDate") ? mirrorServer.get("lastSeenDate").getAsLong() : null);
    }
}