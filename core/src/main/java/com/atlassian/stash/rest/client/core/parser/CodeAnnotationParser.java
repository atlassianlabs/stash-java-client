package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.CodeAnnotation;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.function.Function;

import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getOptionalJsonEnum;
import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getOptionalJsonInt;
import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getOptionalJsonString;
import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getRequiredJsonEnum;
import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getRequiredJsonString;

public class CodeAnnotationParser implements Function<JsonElement, CodeAnnotation> {
    @Override
    public CodeAnnotation apply(JsonElement jsonElement) {
        if (jsonElement == null || !jsonElement.isJsonObject()) {
            return null;
        }
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        return new CodeAnnotation(
                getRequiredJsonString(jsonObject, "reportKey"),
                getOptionalJsonString(jsonObject, "externalId"),
                getRequiredJsonEnum(jsonObject, "severity", CodeAnnotation.Severity.class),
                getRequiredJsonString(jsonObject, "message"),
                getRequiredJsonString(jsonObject, "path"),
                getOptionalJsonInt(jsonObject, "line"),
                getOptionalJsonString(jsonObject, "link"),
                getOptionalJsonEnum(jsonObject, "type", CodeAnnotation.Type.class));
    }
}
