package com.atlassian.stash.rest.client.core.entity;

import com.atlassian.stash.rest.client.api.entity.ReportDataEntry;
import com.google.gson.JsonObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.stash.rest.client.core.entity.RequestUtil.addOptionalJsonProperty;
import static com.atlassian.stash.rest.client.core.entity.RequestUtil.addRequiredJsonProperty;

public class ReportDataEntryRequest {
    @Nonnull
    private final String title;
    @Nonnull
    private final Object value;
    @Nullable
    private final ReportDataEntry.Type type;

    public ReportDataEntryRequest(@Nonnull ReportDataEntry entry) {
        this(entry.getTitle(),
                entry.getValue(),
                entry.getType());
    }

    private ReportDataEntryRequest(@Nonnull String title, @Nonnull Object value, @Nullable ReportDataEntry.Type type) {
        this.title = title;
        this.value = value;
        this.type = type;
    }

    public JsonObject toJson() {
        final JsonObject jsonObject = new JsonObject();
        addRequiredJsonProperty(jsonObject, "title", title);
        addRequiredJsonProperty(jsonObject, "value", value);
        addOptionalJsonProperty(jsonObject, "type", type);
        return jsonObject;
    }
}
