package com.atlassian.stash.rest.client.core.entity;

import com.atlassian.stash.rest.client.api.entity.CodeAnnotation;
import com.google.gson.JsonObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.stash.rest.client.core.entity.RequestUtil.addOptionalJsonProperty;
import static com.atlassian.stash.rest.client.core.entity.RequestUtil.addRequiredJsonProperty;

/**
 * Request object for sending a single {@link CodeAnnotation} entity via REST.
 *
 * @see CodeAnnotationRequest#toJson()
 */
public class CodeAnnotationRequest {
    @Nonnull
    private final CodeAnnotation.Severity severity;
    @Nonnull
    private final String message;
    @Nonnull
    private final String path;
    @Nullable
    private final String externalId;
    @Nullable
    private final Integer line;
    @Nullable
    private final String link;
    @Nullable
    private final CodeAnnotation.Type type;

    public CodeAnnotationRequest(@Nonnull CodeAnnotation codeAnnotation) {
        this(codeAnnotation.getSeverity(),
                codeAnnotation.getMessage(),
                codeAnnotation.getPath(),
                codeAnnotation.getExternalId(),
                codeAnnotation.getLine(),
                codeAnnotation.getLink(),
                codeAnnotation.getType());
    }

    private CodeAnnotationRequest(@Nonnull CodeAnnotation.Severity severity,
                                  @Nonnull String message,
                                  @Nonnull String path,
                                  @Nullable String externalId,
                                  @Nullable Integer line,
                                  @Nullable String link,
                                  @Nullable CodeAnnotation.Type type) {
        this.severity = severity;
        this.message = message;
        this.path = path;
        this.externalId = externalId;
        this.line = line;
        this.link = link;
        this.type = type;
    }

    public JsonObject toJson() {
        final JsonObject jsonObject = new JsonObject();
        addRequiredJsonProperty(jsonObject, "severity", severity);
        addRequiredJsonProperty(jsonObject, "message", message);
        addRequiredJsonProperty(jsonObject, "path", path);
        addOptionalJsonProperty(jsonObject, "externalId", externalId);
        addOptionalJsonProperty(jsonObject, "line", line);
        addOptionalJsonProperty(jsonObject, "link", link);
        addOptionalJsonProperty(jsonObject, "type", type);
        return jsonObject;
    }
}
