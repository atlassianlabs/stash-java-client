package com.atlassian.stash.rest.client.core.parser;

import java.util.function.Function;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.log4j.Logger;

import com.atlassian.stash.rest.client.api.entity.ProjectPermission;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public abstract class ProjectPermissionParser<R> implements Function<JsonElement, R> {

    private static final Logger LOG = Logger.getLogger(ProjectPermissionParser.class);

    @Nonnull
    protected JsonObject getPgPermission(@Nullable JsonElement pgPermission) {
        return Preconditions.checkNotNull(pgPermission, "pgPermission").getAsJsonObject();
    }
    @Nonnull
    protected String getNameAttributeFromObject(@Nonnull final JsonObject jsonObject, @Nonnull final String memberName)
    {
        final JsonObject object = Preconditions.checkNotNull(jsonObject.getAsJsonObject(memberName), memberName);
        return Preconditions.checkNotNull(object.get("name"), "name").getAsString();
    }

    @Nullable
    protected ProjectPermission mapProjectPermission(@Nullable final String permissionRaw)
    {
        if (Strings.isNullOrEmpty(permissionRaw)) {
            LOG.warn("raw permission is null or empty");
            return null;
        }

        ProjectPermission permission = null;
        // use endsWith check as API docs don't contain PROJECT_ prefix
        if (permissionRaw.endsWith("READ")) {
            permission = ProjectPermission.PROJECT_READ;
        } else if (permissionRaw.endsWith("WRITE")) {
            permission = ProjectPermission.PROJECT_WRITE;
        } else if (permissionRaw.endsWith("ADMIN")) {
            permission = ProjectPermission.PROJECT_ADMIN;
        } else {
            LOG.warn("Unknown permission: " + permissionRaw);
        }
        return permission;
    }
}
