package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Report;
import com.atlassian.stash.rest.client.api.entity.ReportDataEntry;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getOptionalJsonEnum;
import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getOptionalJsonString;
import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getRequiredJsonString;

public class ReportParser implements Function<JsonElement, Report> {
    @Override
    public Report apply(JsonElement jsonElement) {
        if (jsonElement == null || !jsonElement.isJsonObject()) {
            return null;
        }
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        final JsonElement dataJsonArray = jsonObject.get("data");
        final List<ReportDataEntry> data =
                dataJsonArray != null && dataJsonArray.isJsonArray() ?
                        StreamSupport.stream(dataJsonArray.getAsJsonArray().spliterator(), false)
                                .map(json -> Parsers.reportDataEntryParser().apply(json))
                                .collect(Collectors.toList()) :
                        Collections.emptyList();

        return new Report(
                getRequiredJsonString(jsonObject, "key"),
                getRequiredJsonString(jsonObject, "title"),
                data,
                getOptionalJsonString(jsonObject, "details"),
                getOptionalJsonString(jsonObject, "vendor"),
                getOptionalJsonString(jsonObject, "link"),
                getOptionalJsonString(jsonObject, "logoUrl"),
                getOptionalJsonEnum(jsonObject, "result", Report.Result.class));
    }
}
