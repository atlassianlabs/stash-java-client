package com.atlassian.stash.rest.client.core.parser;

import com.google.gson.JsonElement;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.function.Function;

public class PropertyParser<T> implements Function<JsonElement, T> {
    private final String property;
    private final Function<JsonElement, T> parser;

    public PropertyParser(@Nonnull String property, @Nonnull Function<JsonElement, T> parser) {
        this.property = property;
        this.parser = parser;
    }

    @Override
    public T apply(JsonElement json) {
        return Optional.ofNullable(json)
                .filter(JsonElement::isJsonObject)
                .map(JsonElement::getAsJsonObject)
                .map(j -> j.get(property))
                .map(parser::apply)
                .orElse(null);
    }
}
