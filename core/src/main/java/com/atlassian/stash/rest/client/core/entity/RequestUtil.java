package com.atlassian.stash.rest.client.core.entity;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.stream.Collector;

public class RequestUtil {
    public static void addOptionalJsonProperty(@Nonnull JsonObject jsonObject,
                                               @Nonnull String propertyName,
                                               @Nullable Object property) {
        if (property != null) {
            addJsonProperty(jsonObject, propertyName, property);
        }
    }

    public static void addRequiredJsonProperty(@Nonnull JsonObject jsonObject,
                                               @Nonnull String propertyName,
                                               @Nullable Object property) {
        if (property != null) {
            addJsonProperty(jsonObject, propertyName, property);
        } else {
            throw new IllegalArgumentException(String.format(
                    "Can't add required property %s to JSON object - value is null",
                    propertyName));
        }
    }

    private static void addJsonProperty(@Nonnull JsonObject jsonObject,
                                        @Nonnull String propertyName,
                                        @Nonnull Object property) {
        if (property instanceof JsonElement) {
            jsonObject.add(propertyName, (JsonElement) property);
        } else if (property instanceof Number) {
            jsonObject.addProperty(propertyName, (Number) property);
        } else if (property instanceof Boolean) {
            jsonObject.addProperty(propertyName, (Boolean) property);
        } else if (property instanceof String) {
            jsonObject.addProperty(propertyName, (String) property);
        } else if (property instanceof Character) {
            jsonObject.addProperty(propertyName, (Character) property);
        } else if (property instanceof Enum<?>) {
            jsonObject.addProperty(propertyName, ((Enum<?>) property).name());
        } else {
            throw new IllegalArgumentException(String.format(
                    "Can't add property %s to JSON object - value is of unknown type: %s",
                    propertyName, property.getClass().getName()));
        }
    }

    @Nonnull
    public static Collector<JsonElement, JsonArray, JsonArray> toJsonArrayCollector() {
        return Collector.of(
                JsonArray::new,
                JsonArray::add,
                RequestUtil::mergeJsonArrays);
    }

    @Nonnull
    private static JsonArray mergeJsonArrays(@Nonnull JsonArray array1, @Nonnull JsonArray array2) {
        final JsonArray result = new JsonArray();
        result.addAll(array1);
        result.addAll(array2);
        return result;
    }
}
