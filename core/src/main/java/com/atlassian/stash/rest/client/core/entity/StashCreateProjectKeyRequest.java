package com.atlassian.stash.rest.client.core.entity;


import com.google.gson.JsonObject;

public class StashCreateProjectKeyRequest {

    private final String key;
    private final String name;
    private final String type;
    private final String description;

    public StashCreateProjectKeyRequest(final String key, final String name, final String type, final String description) {
        this.key = key;
        this.name = name;
        this.type = type;
        this.description = description;
    }

    public JsonObject toJson() {
        JsonObject req = new JsonObject();

        req.addProperty("key", key);
        req.addProperty("name", name);
        req.addProperty("type", type);
        req.addProperty("description", description);

        return req;
    }
}
