package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.StashError;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.function.Function;

import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getOptionalJsonString;
import static com.atlassian.stash.rest.client.core.parser.ParserUtil.getRequiredJsonString;

public class ErrorParser implements Function<JsonElement, StashError> {

    @Override
    public StashError apply(JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();
        return new StashError(
                getRequiredJsonString(jsonObject, "message"),
                getOptionalJsonString(jsonObject, "context"),
                getOptionalJsonString(jsonObject, "exceptionName")
        );
    }
}
