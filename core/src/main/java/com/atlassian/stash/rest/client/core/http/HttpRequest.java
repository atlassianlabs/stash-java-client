package com.atlassian.stash.rest.client.core.http;

public class HttpRequest {
    private final String url;
    private final HttpMethod method;
    private final String payload;
    private final boolean anonymous;

    public HttpRequest(String url, HttpMethod method, String payload, boolean anonymous) {
        this.url = url;
        this.method = method;
        this.payload = payload;
        this.anonymous = anonymous;
    }

    public String getUrl() {
        return url;
    }

    public HttpMethod getMethod() {
        return method;
    }

    public String getPayload() {
        return payload;
    }

    public boolean isAnonymous() {
        return anonymous;
    }
}
