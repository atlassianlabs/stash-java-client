package com.atlassian.stash.rest.client.applinks;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.application.stash.StashApplicationType;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.stash.rest.client.core.StashClientImpl;
import com.atlassian.stash.rest.client.api.StashClient;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ApplinkStashClientFactoryImpl implements ApplinkStashClientFactory {
    // ------------------------------------------------------------------------------------------------------- Constants
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(ApplinkStashClientFactoryImpl.class);
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final ApplicationLinkService applicationLinkService;
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods


    public ApplinkStashClientFactoryImpl(ApplicationLinkService applicationLinkService) {
        this.applicationLinkService = applicationLinkService;
    }

    @Override
    @Nonnull
    public Iterable<ApplicationLink> getStashApplicationLinks() {
        final Iterable<ApplicationLink> applicationLinks = applicationLinkService.getApplicationLinks(StashApplicationType.class);
        assert null != applicationLinks;
        return applicationLinks;
    }

    @Override
    @Nullable
    public ApplicationLink getApplicationLink(@Nonnull final String serverKey) {
        try {
            return applicationLinkService.getApplicationLink(new ApplicationId(serverKey));
        } catch (TypeNotInstalledException e) {
            // I'm not reacting to this exception.  It should theoretically never happen. And no one would know what to do
            // with it anyway.  Lets just say we didn't find the link.
            log.warn("Failed to retrieve application link for " + serverKey, e);
            return null;
        }
    }

    @Override
    @Nonnull
    public StashClient getStashClient(@Nonnull ApplicationLink applicationLink,
                                      @Nullable Class<? extends AuthenticationProvider> authenticationProviderClass) {
        return new StashClientImpl(new ApplinkHttpExecutor(applicationLink, authenticationProviderClass));
    }

}
