package com.atlassian.stash.rest.client.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.stash.rest.client.api.StashException;


public class StashCredentialsRequiredException extends StashException {
    private final ApplicationLink applicationLink;

    public StashCredentialsRequiredException(CredentialsRequiredException cause, ApplicationLink applicationLink) {
        super(cause);
        this.applicationLink = applicationLink;
    }

    public ApplicationLink getApplicationLink() {
        return applicationLink;
    }

    @Override
    public CredentialsRequiredException getCause() {
        return (CredentialsRequiredException) super.getCause();
    }
}
